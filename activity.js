db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);



db.fruits.count(
  {onSale: true}
);



db.fruits.count(
  {
    stock: {$gte: 20}
  }
);



db.fruits.aggregate([
   { $match: { onSale: true } },
   {
      $group:
      {
         _id: "$supplier_id",
         avgPrice: { $avg: "$price" }
      }
   }
]);


// getting max price per supplier
db.fruits.aggregate([
   {
      $group:
      {
         _id: "$supplier_id",
         maxPrice: { $max: "$price" }
      }
   }
]);


// getting min price per supplier
db.fruits.aggregate([
   {
      $group:
      {
         _id: "$supplier_id",
         minPrice: { $min: "$price" }
      }
   }
])
